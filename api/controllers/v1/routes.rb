Runnable::Api.controllers 'v1/routes', provides: :json do
  get :index, with: :id do
    route = Route.last

    route.to_json
  end

  post :index do
    payload = MultiJson.load(request.body.read)

    MapRoute.create(payload)

    { status: 'ok' }.to_json
  end
end
