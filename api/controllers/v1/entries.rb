Runnable::Api.controllers :entries, map: 'v1/entries', provides: :json do
  get :index, with: :id do
    authenticate

    athletes = Athlete.eager_graph(:distances)
      .where(distances__route_id: params[:id])
      .order(:name).all

    result = athletes.map do |athlete|
      next if athlete.distances.empty?
      {
        id: athlete.id,
        name: athlete.name,
        distance: {
          id: athlete.distance.id,
          total: athlete.distance.total.to_f,
          updatedAt: athlete.distance.updated_at || athlete.distance.created_at
        },
        route: {
          id: athlete.distance.route_id
        }
      }
    end

    { athletes: result }.to_json
  end

  post :create, map: '' do
    authenticate

    payload = Oj.load(request.body.read, symbol_keys: true)

    athlete = nil

    Athlete.db.transaction do
      athlete = if payload[:athlete].to_i == -1
                  Athlete.create(name: payload[:name])
                else
                  Athlete[payload[:athlete]]
                end

      athlete.add_entry(distance: payload[:distance], route_id: payload[:route][:id])
      athlete.add_distance(total: payload[:distance], route_id: payload[:route][:id])
    end

    distance = Distance.find(route_id: payload[:route][:id], athlete_id: athlete.id)

    result = {
      athlete: {
        id: athlete.id,
        name: athlete.name,
        distance: {
          id: distance.id,
          total: distance.total.to_f,
          updatedAt: distance.created_at
        },
        route: {
          id: distance.route_id
        }
      }
    }

    result.to_json
  end

  put :update, map: ':id' do
    authenticate

    payload = Oj.load(request.body.read, symbol_keys: true)

    distance = Distance.find(route_id: payload[:route][:id], athlete_id: params[:id])
    athlete = distance.athlete

    Athlete.db.transaction do
      athlete.add_entry(distance: payload[:entry], route_id: payload[:route][:id])
      distance.update(total: distance.total + payload[:entry])
    end

    result = {
      athlete: {
        id: athlete.id,
        name: athlete.name,
        distance: {
          id: distance.id,
          total: distance.total.to_f,
          updatedAt: distance.updated_at
        },
        route: {
          id: distance.route_id
        }
      }
    }

    result.to_json
  end
end
