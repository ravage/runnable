Runnable::Api.controllers 'v1/frontend', provides: :json do
  get :index do
    @map_route = @current_route
    @total = Distance.where(route_id: @current_route.id).sum(:total).to_i * 1000

    render 'frontend/index'
  end

  get :distance do
    route = @current_route
    @total = Distance.where(route_id: @current_route.id).sum(:total)
    @runners = Distance.where(route_id: @current_route.id).map(&:contribution).reduce(:+)

    @distance = route.distance / 1000 unless route.nil?
    @total ||= 0

    render 'frontend/distance'
  end

  get :top, with: :count do
    @top = Distance.eager({ athlete: :authentication_providers })
      .where(route_id: @current_route.id).order(Sequel.desc(:total)).limit(params[:count]).all

    render 'frontend/top'
  end

  before do
    @current_route = MapRoute.current
  end
end
