Runnable::Api.controllers :courses, map: 'v1/courses', provides: :json do
  get :show, map: '', with: :id do
    render 'courses/show'
  end
end
