Runnable::Api.controllers 'v1/facebook', provides: :json do
  put :update, map: 'v1/:id/facebook' do
    payload = MultiJson.load(request.body.read)
    payload[:provider] = 'facebook'

    provider = AuthenticationProvider.find(
      provider: 'facebook',
      uid: payload[:id]
    ) || AuthenticationProvider.create_from_provider(params[:id], payload)

    if provider
      MultiJson.dump({ status: 200 })
    else
      halt(422, MultiJson.dump({ status: 422, message: 'Unprocessable Entity' }))
    end
  end
end
