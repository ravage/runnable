Runnable::Api.controllers :athletes, map: 'v1/athletes', provides: :json do
  get :index do
    @athletes = Athlete.order(:name).all

    render 'athletes/index'
  end

  get :show, map: ':athlete_id' do
    athlete = Athlete[params[:athlete_id]]
    entries = Entry.until(athlete)
    dist = Distance.find(athlete_id: athlete.id, route_id: MapRoute.current.id)
    distances = []
    distance = 0

    entries.each do |entry|
      distance += entry.distance
      if entry.athlete_id == athlete.id
        distances << { total: distance, distance: entry.distance }
      end
    end

    result = {
      athlete: {
        id: athlete.id,
        name: athlete.name,
        distance: {
          id: dist.id,
          total: dist.total.to_f,
          updatedAt: dist.updated_at
        },
        entries: distances
      }
    }

    MultiJson.dump(result)
  end

end
