collection @top, object_root: false

node(:id) { |o| o.athlete.id}
node(:name) { |o| o.athlete.name}
node(:image) { |o| o.athlete.image}
node(:url) { |o| o.athlete.url}
node(:distance) { |o| o.total.to_f}
