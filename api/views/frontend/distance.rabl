object false

node(:done) { @total }
node(:total) { @distance || 0 }
node(:runners) { @runners }
