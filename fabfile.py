from fabric.api import run, task, local, cd, env, prefix
from fabric.contrib import files
from fabric.operations import put
from fabric.context_managers import shell_env
from contextlib import contextmanager as _contextmanager

env.hosts = ['direct.famarunners.com']
env.user= 'deployer'
repo = 'deploy'
deploy_path = '/srv/mocas.lsd.di.uminho.pt'
repo_path = '/home/deployer/mocas.git'
production_files = {
    'alembic.production.ini': 'alembic.ini',
    'production.py': 'production.py'
}
env.activate = 'source {}/.venv/bin/activate'.format(deploy_path)

@_contextmanager
def virtualenv():
    with cd(deploy_path):
        with prefix(env.activate):
            yield


@task
def setup():
    if not files.exists(repo_path):
        run('mkdir {}'.format(repo_path))
        with cd(repo_path):
            run('git init --bare')

        with cd(deploy_path):
            run('virtualenv .venv')

        local('git remote add {repo} {username}@{host}:{repo_path}'.format(
            username=env.user,
            repo=repo,
            host=env.hosts[0],
            repo_path=repo_path))


@task
def push():
    local('git push {} master'.format(repo))


@task
def clone():
    if not files.exists('{}/.git'.format(deploy_path)):
        run('git clone {repo} {deploy}'.format(
            repo=repo_path, deploy=deploy_path))


@task
def pull():
    with cd(deploy_path):
        run('git pull')


@task
def copy_production_files():
    with cd(deploy_path):
        for key, value in production_files.iteritems():
            put(key, value, mode=0600)


@task
def alembic():
    with virtualenv():
        with shell_env(PYTHONPATH=deploy_path):
            run('alembic upgrade head')


@task
def reload_server():
    if files.exists('{path}/master.pid'.format(path=deploy_path)):
        with virtualenv():
            run('uwsgi --reload {path}/master.pid'.format(path=deploy_path))

@task
def pip():
    with virtualenv():
        run('pip install -r requirements.txt')

@task
def deploy():
    push()
    clone()
    pull()
    copy_production_files()
    alembic()
    reload_server()

