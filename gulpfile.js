/* jshint strict: false */

var gulp        = require('gulp');
var util        = require('gulp-util');
var sass        = require('gulp-sass');
var concat      = require('gulp-concat');
var browserify  = require('browserify');
var source      = require('vinyl-source-stream');
var uglify      = require('gulp-uglify');
var buffer      = require('vinyl-buffer');
var riotify    = require('riotify');
var babelify    = require('babelify');
var sourcemaps  = require('gulp-sourcemaps');

var deps        = ['riot', 'lodash', 'ampersand-router', 'reflux', 'superagent', 'q', 'revalidator'];

var paths = {
  css: {
    input:  [
      './assets/scss/**/*.scss'
    ],
    output: './public/assets/css'
  },

  js: {
    input:  './assets/js/app.js',
    output: './public/assets/js',
    all:    ['./assets/js/**/*.js', './app/assets/js/**/*.tag']
  },

  templates: {
    input:  './assets/js/components/**/*.hbs',
    output: './public/assets/templates'
  },

};

var external = {
  jquery: {
    path: './bower_components/jquery/dist/jquery.js',
    expose: 'jquery'
  },

  'socket.io': {
    path: './bower_components/socket.io-client/socket.io.js',
    expose: 'socket.io'
  }
};

function errorHandler(error) {
  util.log(util.colors.red(error.message));
  this.emit('end');
}

gulp.task('css', function() {
  return gulp.src(paths.css.input)
  .pipe(sass({ outputStyle: 'compressed', includePaths: ['bower_components'] }))
  .pipe(concat('bundle.css'))
  .pipe(gulp.dest(paths.css.output));
});

gulp.task('js', function() {
  return browserify({
    debug: true,
    entries: paths.js.input,
    extensions: ['.js', '.tag'],
    paths: ['./assets/js/components']
  })
  .transform(riotify, { type: 'es6' })
  .transform(babelify)
  .external(deps)
  .bundle().on('error', errorHandler)
  .pipe(source('bundle.js'))
  .pipe(buffer())
  .pipe(sourcemaps.init({ loadMaps: true }))
  // .pipe(uglify())
  .pipe(sourcemaps.write('./'))
  .pipe(gulp.dest(paths.js.output));
});

gulp.task('vendor:js', function() {
  return browserify()
  .require(require.resolve(external.jquery.path), { expose: external.jquery.expose })
//  .require(require.resolve(external['socket.io'].path), { expose: external['socket.io'].expose })
  .require(deps)
  .bundle().on('error', errorHandler)
  .pipe(source('vendor.js'))
  .pipe(buffer())
  .pipe(uglify())
  .pipe(gulp.dest(paths.js.output));
});

// gulp.task('templates', function(){
//   return gulp.src(paths.templates.input)
//   .pipe(minifyHtml())
//   .pipe(handlebars())
//   .pipe(wrap('Handlebars.template(<%= contents %>)'))
//   .pipe(declare({
//     namespace: 'App.templates',
//     noRedeclare: true,
//     processName: function(filePath) {
//       return declare.processNameByPath(filePath.replace('assets/js/components', ''));
//     }
//   }))
//   .pipe(concat('templates.js'))
//   .pipe(uglify())
//   .pipe(gulp.dest(paths.templates.output));
// });

gulp.task('default', ['vendor:js', 'js', 'css'], function() {
  gulp.watch(paths.css.input, ['css']);
  gulp.watch(paths.js.all, ['js']);
  // gulp.watch(paths.templates.input, ['templates']);
});


