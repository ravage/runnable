import Base from './base';

class Entry extends Base {
  constructor() {
    super();

    this.url = '/api/v1/entries';
  }
}

export default new Entry();
