import Base from './base';

class Frontend extends Base {
  constructor() {
    super();

    this.url =  '/api/v1/frontend';
  }

  distance() {
    return this.req([this.url, 'distance'].join('/'));
  }

  top(count) {
    return this.req([this.url, 'top', count].join('/'));
  }
}

export default new Frontend();
