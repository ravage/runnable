import request from 'superagent';
import csrf from '../extensions/csrf';
import Q from 'q';

csrf(request);

class Base {
  constructor() {
    this.idProperty = 'id';
    this.url = '/api';
  }

  all() {
    var defer = Q.defer();

    request.get(this.url)
      .end(function(error, response) {
        if (response.ok) {
          defer.resolve(response);
        } else {
          defer.reject(response);
        }
      });

    return defer.promise;
  }

  get(id) {
    var defer = Q.defer();

    request.get([this.url, id].join('/'))
      .accept('json')
      .end(function(error, response) {
        if (response.ok) {
          defer.resolve(response);
        } else {
          defer.reject(response);
        }
      });

    return defer.promise;
  }

  endpoint() {
    return this.isNew() ? this.url : [this.url, this.model[this.idProperty]].join('/');
  }

  isNew() {
    return !this.model.hasOwnProperty(this.idProperty);
  }

  method() {
    return this.isNew() ? 'post' : 'put';
  }

  save(model) {
    var defer = Q.defer();

    this.model = model;

    request[this.method()](this.endpoint())
      .accept('json')
      .csrf()
      .send(model)
      .end(function(error, response) {
        if (response.ok) {
          defer.resolve(response);
        } else {
          defer.reject(response);
        }
      });

    return defer.promise;
  }

  req(endpoint, data) {
    var defer = Q.defer();

    data = data || {};

    request.get(endpoint)
      .accept('json')
      .csrf()
      .send(data)
      .end(function(error, response) {
        if (response.ok) {
          defer.resolve(response);
        } else {
          defer.reject(response);
        }
      });

    return defer.promise;
  }
}

export default Base;
