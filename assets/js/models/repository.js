import athlete from './athletes';
import route from './routes';
import frontend from './frontend';
import entry from './entry';

class Repository {
  constructor() {
    this.repositories = {};
  }

  get(key) {
    return this.repositories[key];
  }

  register(key, repository) {
    this.repositories[key] = repository;
  }
}

var repository = new Repository();

repository.register('athletes', athlete);
repository.register('routes', route);
repository.register('frontend', frontend);
repository.register('entries', entry);

export default repository;
