import Base from './base';

class Athlete extends Base {
  constructor() {
    super();

    this.url = '/api/v1/athletes';
  }
}

export default new Athlete();
