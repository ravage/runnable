import Base from './base';

class Route extends Base {
  constructor() {
    super();

    this.url = '/api/v1/routes';
  }
}

export default new Route();