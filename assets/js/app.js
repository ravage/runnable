/* global FB:true */
require('../scss/app.scss');

import '../../bower_components/epoly/v3_epoly.js';
import AmpersandRouter from 'ampersand-router';
import riot from 'riot';

import './components/map/frontend.tag';
import './components/map/top-bar.tag';
import './components/map/top-athletes.tag';
import './components/facebook/login.tag';
import './components/facebook/run.tag';

class Router extends AmpersandRouter {
  constructor() {
    super();

    this.routes = {
      '':   'root',
      'athletes':       'athletesIndex',
      'athletes/:id':   'athletesShow'
    };

    this._bindRoutes();
  }

  root() {
    riot.mount('*');
  }

  athletesIndex() {
    this.loadFacebook();
    riot.mount('top-bar, top-athletes');
  }

  athletesShow() {
    // this.loadFacebook();
    riot.mount('top-bar, top-athletes, map');
  }

  loadFacebook() {
    window.fbAsyncInit = function() {
      FB.init({
        appId      : '916009131796874',
        xfbml      : false,
        version    : 'v2.3'
      });

      riot.mount('facebook-login');
    };

    (function(d, s, id){
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) {return;}
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/pt_PT/sdk.js";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
  }
}

var router = new Router();
router.history.start({ pushState: true });
