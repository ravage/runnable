<facebook-login>
  <a href="#" show={!linked} onClick={loginHandler}><i class="fa fa-facebook-official"></i></a>
  <i class="fa fa-link" show={linked}></i>

  <script>
    /* global FB: true */

    import request from 'superagent';
    import csrf from '../../extensions/csrf';

    csrf(request);

    this.linked = this.opts.linked === 'true' ? true : false;

    this.loginHandler = () => {
      FB.login((response) => {
        if (response.status === 'connected') {
          FB.api('/me', this.associate);
        }
      }, { scope: 'public_profile,email,publish_actions' });
    };

    this.associate = (response) => {
      request.put(['/api/v1', this.opts.user, 'facebook'].join('/'))
        .send(response)
        .csrf()
        .end(() => {
          this.linked = true;
          this.update();
        });
    };

  </script>
</facebook-login>
