<facebook-run>
  <a href="javascript:void(0)" onClick={shareHandler}><i class="fa fa-share-alt"></i></a>

  <script>
    /* global FB:true */
    import request from 'superagent';

    this.shareHandler = () => {
      var course = {
        'fb:app_id': '000000000',
        'og:type': 'fitness.course',
        'og:url': window.location.href,
        'og:title': 'Teste!',
        'fitness:distance:value': 10,
        'fitness:distance:units': 'km',
        'fitness:metrics': [ {
          'location': {
            'latitude': 41.41103,
            'longitude': -8.5236800000
          }
        },
        {
          'location': {
            'latitude': 41.41169,
            'longitude':  -8.52357000
          }
        }
        ]
      };

      FB.login((response) => {
        FB.api(
          "/me/fitness.runs",
          "POST",
          {
            course: course
          },
          function (response) {
          }
        );
      }, { scope: 'public_profile,email,publish_actions' });

    };
  </script>
</facebook-run>
