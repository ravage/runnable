import Reflux from 'reflux';

var actions = Reflux.createActions(['create', 'update', 'load']);

actions.loadRequest = Reflux.createAction({ asyncResult: true });
actions.createRequest = Reflux.createAction({ asyncResult: true });
actions.updateRequest = Reflux.createAction({ asyncResult: true });

export default actions;
