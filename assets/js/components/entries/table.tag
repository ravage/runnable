<entries-table>
  <div class="row">
    <div class="small-12 columns">
      <h2>Entradas</h2>
      <table class="u-full-width">
        <thead>
          <tr>
            <th>ID</th>
            <th>Atleta</th>
            <th>Distância (km)</th>
            <th>Última Entrada</th>
            <th width="150">Adicionar (km)</th>
          </tr>
        </thead>
        <tbody>
          <tr each={athlete, i in opts.athletes }>
            <td>{ athlete.id }</td>
            <td>{ athlete.name }</td>
            <td>{ athlete.distance.total.toLocaleString('pt-PT') }</td>
            <td>{ athlete.distance.updatedAt }</td>
            <td>
              <form onSubmit={ parent.handleSubmit }>
                <input
                data-id={ i }
                type="text"
                name="distance"
                class="radius"
                placeholder="Distância em km" />
              </form>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>

  <script>
    import actions from './actions';

    this.index = -1;

    this.handleSubmit = (e) => {
      var athlete = e.item.athlete;
      var input = e.target.distance;

      athlete.entry = parseFloat(input.value.replace(',', '.'));

      if (athlete.entry > 0.0) {
        this.index = e.target.distance.getAttribute('data-id');
        actions.update(athlete);
      }

      e.preventDefault();
    };

    this.on('updated', () => {
      var selector = `athletes-table input[data-id="${this.index}"]`;
      var input = document.querySelector(selector);

      if (input) {
        input.focus();
      }
    });
  </script>
</entries-table>
