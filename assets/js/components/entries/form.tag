<athletes-form>
  <div class="row">
    <div class="small-12 columns">
      <h2>Novo Atleta</h2>
      <form name="form" onSubmit={ handleSubmit }>
        <div class="row">
          <div class="medium-4 columns">
            <select name="existing-name">
              <option each={ athletes } value={ athlete.id }>{ athlete.name }</option>
            </select>
          </div>
          <div class="medium-4 columns">
            <input type="text" name="name">
          </div>
          <div class="medium-4 columns end">
            <div class="row collapse">
              <div class="small-9 columns">
                <input
                type="text"
                name="distance"
                class="radius"
                placeholder="Distância em km" />
              </div>
              <div class="small-3 columns">
                <button class="button radius postfix">Adicionar</button>
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>

  <script>
    import actions from './actions';
    import store from './store';
    import athleteActions from '../athletes/actions';
    import athletesStore from '../athletes/store';
    import validator from '../../validators/validator';
    import schema from './add-athlete-form-schema';

    this.on('mount', () => {
      this.unsubscribe = athletesStore.listen(this.handleStore);
      athleteActions.load();
    });

    this.on('unmount', () => {
      this.unsubscribe();
    });

    this.handleStore = (response) => {
      this.athletes = response.payload;
      this.athletes.unshift({ athlete: { id: -1, name: 'Selecione Atleta'} });
      this.update();
    };

    this.handleSubmit = (e) => {
      var distance = this.distance.value;

      var model = {
        name: this.name.value,
        athlete: this['existing-name'].value,
        distance: parseFloat(distance.replace(',', '.')),
        route: {
          id: this.opts.route
        }
      };

      var validation = validator.validate(model, schema);

      if (validation.valid && (model.athlete !== '-1' || model.name.length >= 3)) {
        actions.create(model);
        this.form.reset();
        this.name.focus();
      }

      e.preventDefault();
    };

  </script>
</athletes-form>
