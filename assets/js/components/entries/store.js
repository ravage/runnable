import Reflux from 'reflux';
import request from 'superagent';
import actions from './actions';
import csrf from '../../extensions/csrf';
import repository from '../../models/repository';
import {findIndex} from 'lodash';

csrf(request);

var store = Reflux.createStore({
  init: function() {
    this.listenToMany(actions);
    this.athletes = [];
  },

  onCreate: function(model) {
    actions.createRequest.promise(repository.get('entries').save(model));
  },

  onLoad: function(id) {
    actions.loadRequest.promise(repository.get('entries').get(id));
  },

  onUpdate: function(model) {
    actions.updateRequest.promise(repository.get('entries').save(model));
  },

  onAthletes: function() {
    actions.athletesRequest.promise(repository.get('athletes').all());
  },

  // async callbacks
  onCreateRequestCompleted: function(response) {
    this.athletes.push(response.body.athlete);
    this.update();
  },

  onUpdateRequestCompleted: function(response) {
    var index = findIndex(this.athletes, athlete => athlete.id === response.body.athlete.id);
    this.athletes[index] = response.body.athlete;
    this.update();
  },

  onLoadRequestCompleted: function(response) {
   this.athletes = response.body.athletes;
   this.update();
  },

  onAthletesRequestCompleted: function(response) {
   this.trigger({ payload: response.body });
  },

  onLoadRequestFailed: function(error) {
    this.trigger({ action: 'error' });
  },

  onLoadAthletesRequestFailed: function(error) {
    this.trigger({ action: 'error' });
  },

  update: function() {
    this.trigger({ payload: this.athletes });
  }
});

export default store;
