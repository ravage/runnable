export default {
  properties: {
    name: {
      type: 'string',
      messages: {
        minLength: 'Deve conter pelo menos 3 carateres.',
        required: 'Campo obrigatório.'
      }
    },

    distance: {
      type: 'number',
      required: true,
      minimum: 1,
      maximum: 99999,
      messages: {
        required: 'Campo obrigatório.',
        type: 'Deve conter um valor numérico.',
        minimum: 'Valor deve ser superior a 0.',
        maximum: 'Valor não deve exceder 99999.'
      }
    }
  }
};
