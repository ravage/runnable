<entries-index>
  <div class="row">
    <div class="small-12 columns">
      <h1><small>{{ opts.title }}</small></h1>
    </div>
  </div>
  <athletes-form route="{ opts.id }"/>
  <entries-table athletes="{ athletes }" />

  <script>
    import store from './store';
    import actions from './actions';

    this.on('mount', () => {
      this.unsubscribe = store.listen(this.handleStore);
      actions.load(this.opts.id);
    });

    this.on('unmount', () => {
      this.unsubscribe();
    });

    this.handleStore = (response) => {
      this.athletes = response.payload;
      this.update();
    };
  </script>
</entries-index>
