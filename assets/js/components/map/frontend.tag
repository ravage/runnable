<map>
  <div id="map-canvas"></div>

  <script>
    /*global google */

    import repository from '../../models/repository';
    import ImageMarker from '../../extensions/image-marker';
    import numeral from '../../extensions/numeral';

    var polyline = null;

    var mapOptions = {
      center: { lng: -7.8536599, lat: 39.557191 },
      disableDefaultUI: true,
      zoom: 8
    };
    var map = new google.maps.Map(this['map-canvas'], mapOptions);

    this.model = {};

    this.on('mount', () => {
      var result = repository.get('frontend').all();
      var bounds = new google.maps.LatLngBounds();

      result.then((response) => {
        var positions = response.body.route.positions;
        var position;
        var donePolyline;

        polyline = new google.maps.Polyline({
          path: [],
          strokeColor: '#FF0000',
          strokeWeight: 6,
          strokeOpacity: 0.6
        });

        positions.forEach(function(position) {
          position = new google.maps.LatLng(position.latitude, position.longitude);
          polyline.getPath().push(position);
          bounds.extend(position);
        });

        donePolyline = new google.maps.Polyline({
          path: [],
          strokeColor: '#00FF00',
          strokeWeight: 6,
          strokeOpacity: 0.6
        });

        var done = polyline.GetIndexAtDistance(response.body.done);
        var length = polyline.getPath().getLength();

        for (var i = 0; i <= done; i++) {
          donePolyline.getPath().push(polyline.getPath().getAt(i));
        }

        polyline.setMap(map);
        donePolyline.setMap(map);
        map.fitBounds(bounds);

        if (this.opts.hasOwnProperty('entries')) {
          this.showEntries(this.opts.entries);
        }

      });
    });

    this.showEntries = () => {
      var result = repository.get('athletes').get(this.opts.entries);

      result.then((response) => {
        var bounds = new google.maps.LatLngBounds();
        var entries = response.body.athlete.entries;
        var startPosition;
        var endPosition;
        var markerPosition;
        var startMeters;
        var endMeters;
        var marker;

        if (entries.length === 1) {
          bounds.extend(polyline.getPath().getAt(0));
        }

        entries.forEach((entry) => {
          startMeters = (entry.total - entry.distance) * 1000;
          endMeters = entry.total * 1000;
          startPosition = polyline.GetIndexAtDistance(startMeters);
          endPosition = polyline.GetIndexAtDistance(endMeters);
          markerPosition = polyline.getPath().getAt(endPosition);
          bounds.extend(markerPosition);

          marker = new ImageMarker({
            distance: numeral(entry.distance).format() + ' km',
            image: '/assets/map-athlete.png',
            map: map,
            position: markerPosition
          });

          this.paintDistance(startPosition, endPosition);
        });
        map.fitBounds(bounds);
      });

      this.paintDistance = (from, to) => {
        var i;
        var path = [];
        var distancePolyline;

        for (i = from; i <= to; i++) {
          path.push(polyline.getPath().getAt(i));
        }

        distancePolyline = new google.maps.Polyline({
          path: path,
          strokeColor: '#0000FF',
          strokeWeight: 6,
          strokeOpacity: 0.6
        });

        distancePolyline.setMap(map);
      };
    };
  </script>
</map>
