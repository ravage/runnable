<map>
  <div id="map-canvas"></div>

  <div class="map-directions">
    <form action="" method="post" name="directions" onSubmit={directionsHandler}>
      <input class="radius" type="text" placeholder="Partida" name="from">
      <input class="radius" type="text" placeholder="Destino" name="to">
      <input type="submit" value="Direções" class="tiny radius button">
      <button onClick={saveHandler} disabled={!isReady()} class="tiny radius green success">
        <i class="fa fa-plus"></i>
      </button>
    </form>
  </div>

  <script>
    /*global google */

    import repository from '../../models/repository';

    var polyline = null;
    var mapOptions = {
      center: { lat: -34.397, lng: 150.644},
      disableDefaultUI: true,
      zoom: 8
    };
    var map = new google.maps.Map(this['map-canvas'], mapOptions);

    this.on('mount', function() {
      var result = repository.get('routes').get(7);

      result.then(function(response) {
        var positions = response.body.positions;
        polyline = new google.maps.Polyline({
          path: [],
          strokeColor: '#FF0000',
          strokeWeight: 3
        });

        positions.forEach(function(position) {
          polyline.getPath().push(new google.maps.LatLng(position.latitude, position.longitude));
        });

        polyline.setMap(map);
      });
    });

    this.model = {};

    this.isReady = () => {
      return !!this.model.positions && !!this.model.positions.length;
    };

    this.saveHandler = (e) => {
      repository.get('routes').save(this.model);
    };

    this.directionsHandler = (e) => {
      if (this.from.value === '' || this.to.value === '') {
        return;
      }

      if (polyline) {
        polyline.setMap(null);
      }

      var directions = new google.maps.DirectionsService();
      directions.route({
        origin: this.from.value,
        destination: this.to.value,
        travelMode: google.maps.TravelMode.DRIVING,
        unitSystem: google.maps.UnitSystem.METRIC,

      }, function(response, status) {

        if (status === google.maps.DirectionsStatus.OK) {
          polyline = new google.maps.Polyline({
            path: [],
            strokeColor: '#FF0000',
            strokeWeight: 3
          });

          var bounds = new google.maps.LatLngBounds();
          var legs = response.routes[0].legs;

          legs.forEach(function(leg) {
            leg.steps.forEach(function(step) {
              step.path.forEach(function(segment) {
                polyline.getPath().push(segment);
                bounds.extend(segment);
              });
            });
          });

          polyline.setMap(map);
          map.fitBounds(bounds);

          this.model = this.hydrate(polyline.getPath().getArray(), legs[0]);

          this.update();
        }
      }.bind(this));
    };

    this.hydrate = (segments, route) => {
      var positions = [];
      var model;

      segments.forEach(function(position) {
        positions.push({
          latitude: position.lat().toFixed(6),
          longitude: position.lng().toFixed(6)
        });
      });

      return {
        positions: positions,
        from: route.start_address,
        to: route.end_address,
        distance: route.distance.value
      };
    };
  </script>
</map>
