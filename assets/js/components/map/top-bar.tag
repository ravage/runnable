<top-bar class="top-bar">
  <div class="row">
    <div class="small-12 columns">
      <a href="/">
        <img src="/assets/world-logo.png" alt="World Logo">
        <img src="/assets/letters-logo.png" alt="World Logo">
      </a>

      <div class="top-info">
        <i class="top-info-item fa fa-check-circle-o"></i> {model.done} km
        <i class="top-info-item fa fa-flag-checkered"></i> {model.total} km
        <i class="fa fa-euro top-info-item"></i> {model.runners}
        <a class="top-info-item" href="#" onClick={showTopTen}><i class="fa fa-trophy"></i> 10</a>
        <a class="top-info-item" href="/athletes"><i class="fa fa-bars"></i></a>
        <a class="top-info-item" href="https://www.facebook.com/runnablebyfamarunners"><i class="fa fa-facebook-official"></i></a>
      </div>
    </div>
  </div>

  <script>
    import repository from '../../models/repository';
    import actions from './actions';
    import numeral from '../../extensions/numeral';

    this.model = {
      done: 0,
      total: 0,
      runners: 0
    };

    this.showTopTen = (e) => {
      actions.showTop(e.currentTarget);
    };

    this.on('mount', () => {
      var result = repository.get('frontend').distance();

      result.then(function(response) {
        var payload = response.body;
        var toGo = payload.total - payload.done;
        var achievementElement = null;

        if (toGo < 0) {
          toGo = 0;
          achievementElement = document.querySelector('.achievement');

          if (achievementElement) {
            achievementElement.classList.remove('hide');
          }
        }

        this.model = {
          done: numeral(payload.done).format(),
          total: numeral(toGo).format(),
          runners: payload.runners
        };

        this.update();
      }.bind(this));
    });
  </script>
</top-bar>
