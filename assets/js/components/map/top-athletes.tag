<top-athletes>
  <ul class="list" show={show}>
    <li each={athletes} class="item">
      <a href="/athletes/{id}">
        <span class="info">
          <img class="circular" src="{image}" alt="Atleta"> {parent.numeral(distance).format()} km
        </span>
        {name}
      </a>
    </li>
  </ul>

  <script>
    import actions from './actions';
    import store from './store';
    import repository from '../../models/repository';
    import numeral from '../../extensions/numeral';

    this.athletes = [];
    this.show = false;
    this.origin = undefined;
    this.numeral = numeral;

    this.on('mount', () => {
      var result = repository.get('frontend').top(10);

      this.unsubscribe = store.listen(this.handleStore);

      result.then((response) => {
        this.athletes = response.body;
        this.update();
      }.bind(this));
    });

    this.on('unmount', () => {
      this.unsubscribe();
    });

    this.handleStore = (e) => {
      this.origin = e;
      this.show = !this.show;
      this.handleResize();
      this.update();
    };

    this.on('unmount', () => {
      this.unsubscribe();
    });

    this.handleResize = () => {
      if (this.show) {
        this.root.style.left = this.origin.getBoundingClientRect().left + 'px';
        this.root.style.top = this.origin.getBoundingClientRect().top + 65 + 'px';
      }
    };

    window.addEventListener('resize', () => {
      this.handleResize();
    }.bind(this));
  </script>
</top-athletes>
