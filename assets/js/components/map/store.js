import Reflux from 'reflux';
import actions from './actions';

var store = Reflux.createStore({
  init: function() {
    this.listenToMany(actions);
  },

  onShowTop: function(e) {
    this.trigger(e);
  }
});

export default store;
