import Reflux from 'reflux';

var actions = Reflux.createActions(['showTop']);

export default actions;
