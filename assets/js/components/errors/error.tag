<form-error>
    { error }

    <script>
        this.on('update', () => {
            console.log('ERROR');
            this.opts.errors.some(error => {
                if (error.property === this.opts.field) {
                    this.error  = error.message;
                    return true;
                }

                return false;
            });
        });

        // this.isInvalid = (e) => {
        //     console.log('invalid');
        //     return this.errors.some(error => error.property === e);
        // }
        this.isInvalid = true;
    </script>
</form-error>
