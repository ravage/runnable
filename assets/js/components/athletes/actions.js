import Reflux from 'reflux';

var actions = Reflux.createActions(['load']);

actions.loadRequest = Reflux.createAction({ asyncResult: true });

export default actions;
