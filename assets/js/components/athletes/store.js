import Reflux from 'reflux';
import actions from './actions';
import repository from '../../models/repository';

var store = Reflux.createStore({

  init() {
    this.listenToMany(actions);
    this.athletes = [];
  },

  onLoad() {
    actions.loadRequest.promise(repository.get('athletes').all());
  },

  onLoadRequestCompleted(response) {
    this.athletes = response.body;
    this.update();
  },

  update() {
    this.trigger({ payload: this.athletes });
  }

});

export default store;
