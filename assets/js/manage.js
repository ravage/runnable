import '../scss/manage.scss';

import AmpersandRouter from 'ampersand-router';
import riot from 'riot';

import './components/entries/index.tag';
import './components/entries/table.tag';
import './components/entries/form.tag';
import './components/map/backend.tag';

class Router extends AmpersandRouter {
  constructor() {
    super();

    this.routes = {
      'manage/entries/:id':   'entriesShow',
      'manage/map-routes':    'mapRoutes'
    };

    this._bindRoutes();
  }

  entriesShow(id) {
    riot.mount('*', { id: id });
  }

  mapRoutes() {
    riot.mount('*');
  }

}

var router = new Router();
router.history.start({ pushState: true });

setTimeout(function() {
  var el = document.querySelectorAll('.alert-box');
  var i = 0;

  for (i = 0; i < el.length; i++) {
    el[i].remove();
  }
}, 5000);
