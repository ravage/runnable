import numeral from 'numeral';

numeral.language('pt-pt', {
  delimiters: {
    thousands: ' ',
    decimal: ','
  }
});

numeral.language('pt-pt');
numeral.defaultFormat('0.[00]');

export default numeral;
