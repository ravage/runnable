function csrf(token) {
  var element = document.querySelector('meta[name=csrf-token]');

  if (element) {
    token = element.getAttribute('content');
  }

  this.set('X-CSRF-Token', token);

  return this;
}

export default function(superagent) {
  var Request = superagent.Request;

  Request.prototype.csrf = csrf;

  return superagent;
}
