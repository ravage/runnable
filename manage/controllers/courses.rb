Runnable::Manage.controllers :courses, map: 'courses' do
  get :index do
    @courses = MapRoute.all
    render 'courses/index'
  end

  before do
    authenticate
  end
end
