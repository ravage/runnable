Runnable::Manage.controllers do
  get :index do
    render 'site/index'
  end

  before do
    authenticate
  end
end
