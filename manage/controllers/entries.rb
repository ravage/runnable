Runnable::Manage.controller :entries, map: 'entries' do
  get :show, map: '', with: :id do
    @course = MapRoute[params[:id]]
    render 'entries/show'
  end

  before do
    authenticate
  end
end
