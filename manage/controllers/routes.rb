Runnable::Manage.controllers :routes, map: 'map-routes' do
  get :index do
    render :index
  end

  before do
    authenticate
  end
end
