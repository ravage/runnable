class User < Sequel::Model
  include BCrypt

  def self.authenticate(email:, password:)
    user = User.first(email: email)

    if user && Password.new(user.encrypted_password) == password
      user
    end
  end

  def password=(value)
    self.encrypted_password = Password.create(value)
  end
end
