class Athlete < Sequel::Model
  set_allowed_columns :name

  one_to_many :entries
  one_to_many :distances
  one_to_many :authentication_providers

  def linked?
    authentication_providers.length > 0
  end

  def provider
    @provider ||= authentication_providers.select { |p| p.provider == 'facebook' }.first
  end

  def image
    provider.nil? ? '/assets/unlinked.png' : "//graph.facebook.com/#{provider.uid}/picture"
  end

  def url
    provider.nil? ? 'javascript:void(0)' : "https://facebook.com/#{provider.uid}"
  end

  def distance
    distances.last
  end
end
