class Entry < Sequel::Model
  many_to_one :athlete

  def self.until(athlete)
    with_sql('SELECT * FROM entries
             WHERE id <= (
             SELECT MAX(id) FROM entries
             WHERE athlete_id = ?
             AND route_id = (SELECT id FROM routes WHERE state = 1))
             AND route_id = (SELECT id FROM routes WHERE state = 1) ORDER BY id', athlete.id)
  end
end
