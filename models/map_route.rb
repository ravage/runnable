class MapRoute < Sequel::Model(:routes)
  def positions=(value)
    self[:positions] = MultiJson.dump(value)
  end

  def positions
    MultiJson.load(self[:positions])
  end

  def self.current
    find(state: 1)
  end
end
