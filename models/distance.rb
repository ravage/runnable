class Distance < Sequel::Model
  MULTIPLE = 50
  MAX_CONTRIBUTION = 5

  many_to_one :athlete

  def contribution
    limit = MULTIPLE * MAX_CONTRIBUTION

    if total >= limit
      MAX_CONTRIBUTION
    else
      total.to_i / MULTIPLE + 1
    end
  end
end
