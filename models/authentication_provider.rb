class AuthenticationProvider < Sequel::Model
  many_to_one :athlete

  def self.create_from_provider(athlete_id, payload)
    athlete = Athlete[athlete_id]

    if athlete
      create do |o|
        o.uid = payload[:id]
        o.email = payload[:email]
        o.name = payload[:name]
        o.provider = payload[:provider]
        o.athlete_id = athlete.id
      end
    end
  end
end
