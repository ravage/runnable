Runnable::App.controllers :athletes do
  get :index do
    @athletes = Athlete.eager_graph(:distances, :authentication_providers)
      .where(distances__route_id: MapRoute.current.id)
      .order(:name).all
    render 'athletes/index'
  end

  get :show, map: 'athletes/:id' do
    @athlete = Athlete[params[:id]]
    render 'athletes/show'
  end
end
