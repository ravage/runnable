var path = require('path');
var webpack = require('webpack');
var ExtractTextPlugin = require("extract-text-webpack-plugin");

module.exports = {
  // resolve: {
  //   root: [path.join(__dirname, "bower_components")]
  // },

  entry: {
    app: './assets/js/app',
    manage: './assets/js/manage',
    vendor: ['ampersand-router', 'lodash', 'q', 'reflux', 'riot', 'superagent', 'revalidator', 'numeral']
  },

  output: {
    filename: '[name].bundle.js',
    path: './public/assets'
  },

  module: {
    loaders: [
      {
        test: /\.tag$/,
        loader: 'tag',
        query: { type: 'es6' },
        include: [path.join(__dirname, 'assets/js')]
      },

      {
        test: /\.js$/,
        loader: 'babel',
        include: [path.join(__dirname, 'assets/js')]
      },

      {
        test: /\.scss|\.css$/,
        loader: ExtractTextPlugin.extract('style-loader', 'css-loader!sass-loader' +
                                          '?includePaths[]=' + (path.resolve(__dirname, 'bower_components')))
      },
      { test: /\.jpe?g$|\.gif$|\.png$|\.svg$|\.woff$|\.ttf$/, loader: 'file' },
    ]
  },

  plugins: [
    new ExtractTextPlugin("[name].bundle.css"),
    new webpack.optimize.CommonsChunkPlugin('vendor', 'vendor.js'),
    // new webpack.ResolverPlugin(
    //   new webpack.ResolverPlugin.DirectoryDescriptionFilePlugin("bower.json", ["main"])
    // )
  ]
};
