class ApiFailureApp
  def call(env)
    response = { status: '401 Unauthorized' }.to_json
    ['401', {'Content-Type' => 'application/json'}, [response]]
  end
end
