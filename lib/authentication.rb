Warden::Strategies.add(:password) do
  def valid?
    params['email'] || params['password']
  end

  def authenticate!
    u = User.authenticate(email: params['email'], password: params['password'])
    u.nil? ? fail!('Could not log in') : success!(u)
  end
end

Warden::Manager.serialize_into_session do |user|
  user.id
end

Warden::Manager.serialize_from_session do |id|
  User[id]
end

