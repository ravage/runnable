##
# This file mounts each app in the Padrino project to a specified sub-uri.
# You can mount additional applications using any of these commands below:
#
#   Padrino.mount('blog').to('/blog')
#   Padrino.mount('blog', :app_class => 'BlogApp').to('/blog')
#   Padrino.mount('blog', :app_file =>  'path/to/blog/app.rb').to('/blog')
#
# You can also map apps to a specified host:
#
#   Padrino.mount('Admin').host('admin.example.org')
#   Padrino.mount('WebSite').host(/.*\.?example.org/)
#   Padrino.mount('Foo').to('/foo').host('bar.example.org')
#
# Note 1: Mounted apps (by default) should be placed into the project root at '/app_name'.
# Note 2: If you use the host matching remember to respect the order of the rules.
#
# By default, this file mounts the primary app which was generated with this project.
# However, the mounted app can be modified as needed:
#
#   Padrino.mount('AppName', :app_file => 'path/to/file', :app_class => 'BlogApp').to('/')
#

##
# Setup global project settings for your apps. These settings are inherited by every subapp. You can
# override these settings in the subapps as needed.
#
Padrino.configure_apps do
  disable :sessions

  set :session_secret, ENV['SESSION_SECRET']
  set :protection, :except => :path_traversal
  set :protect_from_csrf, true
  set :cache, Padrino::Cache.new(:Redis)
  set :session_id, 'runnable'

  Padrino::IGNORE_CSRF_SETUP_WARNING = true
  Padrino.use Rack::Session::Redis, :redis_server => 'redis://127.0.0.1:6379/0', key: 'runnable'
end

# Mounts the core application for this project

Padrino.mount('Runnable::Api', :app_file => Padrino.root('api/app.rb')).to('/api')
Padrino.mount('Runnable::Manage', :app_file => Padrino.root('manage/app.rb')).to('/manage')
Padrino.mount('Runnable::App', :app_file => Padrino.root('app/app.rb')).to('/')
