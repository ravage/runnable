Sequel::Model.plugin(:schema)
Sequel::Model.plugin(:timestamps)
Sequel::Model.plugin(:json_serializer)
Sequel::Model.raise_on_save_failure = false # Do not throw exceptions on failure
Sequel::Model.db = case Padrino.env
  when :development then Sequel.connect("postgres://localhost/runnable_development", :loggers => [logger])
  when :production  then Sequel.connect("postgres://#{ENV['DB_USERNAME']}:#{ENV['DB_PASSWORD']}@localhost/runnable_production",  :loggers => [logger])
  when :test        then Sequel.connect("postgres://localhost/runnable_test",        :loggers => [logger])
end
