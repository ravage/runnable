Sequel.migration do
  change do
    alter_table(:entries) do
      add_foreign_key 'route_id', :routes
      add_index :route_id
    end
  end
end
