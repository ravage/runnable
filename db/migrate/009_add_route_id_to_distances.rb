Sequel.migration do
  change do
    alter_table(:distances) do
      add_foreign_key 'route_id', :routes
      add_index :route_id
    end
  end
end
