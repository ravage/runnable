Sequel.migration do
  change do
    create_table(:entries) do
      primary_key :id
      foreign_key :athlete_id, :athletes, null: false, index: true
      Numeric :distance, size: [7, 2], null: false
      Numeric :latitude, size: [9, 6]
      Numeric :longitude, size: [9, 6]
      Time :created_at
      Time :updated_at
    end
  end
end
