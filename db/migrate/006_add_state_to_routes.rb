Sequel.migration do
  change do
    alter_table(:routes) do
      add_column :state, Fixnum, default: 0
    end
  end
end
