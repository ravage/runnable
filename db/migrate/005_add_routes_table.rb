Sequel.migration do
  change do
    create_table(:routes) do
      primary_key :id
      String :from, size: 255, null: false
      String :to, size: 255, null: false
      Fixnum :distance, null: false
      column :positions, :json, null: false
      Time :created_at
      Time :updated_at
    end
  end
end
