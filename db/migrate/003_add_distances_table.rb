Sequel.migration do
  change do
    create_table(:distances) do
      primary_key :id
      foreign_key :athlete_id, :athletes, null: false, index: true
      Numeric :total, default: 0, size: [9, 2]
      Time :created_at
      Time :updated_at
    end
  end
end
