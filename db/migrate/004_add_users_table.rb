Sequel.migration do
  change do
    create_table(:users) do
      primary_key :id
      String :email, size: 255, null: false
      String :encrypted_password, size: 255, null: false
      String :reset_password_token, size: 255
      Time :reset_password_sent_at
      Time :remember_created_at
      Fixnum :sign_in_count, default: 0
      Time :current_sign_in_at
      Time :last_sign_in_at
      String :current_sign_in_ip
      String :last_sign_in_ip
      Fixnum :failed_attempts
      Time :created_at
      Time :updated_at
    end
  end
end
