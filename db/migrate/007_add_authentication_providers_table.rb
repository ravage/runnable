Sequel.migration do
  change do
    create_table(:authentication_providers) do
      primary_key :id
      String :uid, size: 255, null: false
      String :email, size: 255, null: false
      String :name, size: 255, null: false
      String :provider, size: 255, null: false
      foreign_key :athlete_id, :athletes, null: false
      index :athlete_id
      Time :created_at
      Time :updated_at
    end
  end
end
