Sequel.migration do
  change do
    create_table(:athletes) do
      primary_key :id
      String :name, size: 255, null: false
      Time :created_at
      Time :updated_at
    end
  end
end
